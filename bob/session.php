<?php
header('Access-Control-Allow-Origin: *');



     $json = '
        {
            "webAppId": "webapp-id-example",
            "allowedOrigins": ["*"],
            "urlSchemeDetails": {
                "host": "rmorgan-latest.cafex.com",
                "port": "8443",
                "secure": true
            },
            "voice":
            {
                "username": "bob",
                "displayName": "Bob",
                "domain": "webgateway.example.com",
                "inboundCallingEnabled": true
            },
        
        "aed": {
                "accessibleSessionIdRegex": ".*",
                "maxMessageAndUploadSize": "5000",                                                                                       "dataAllowance": "5000"                                                                   },
                "additionalAttributes":{
                "AED2.metadata":{
                    "role":"agent"
                },
            "AED2.allowedTopic":"%s"
            }
        }
    ';

    // if supplied, use the account details provided
    $username = (empty($_GET['username'])) ? 'user1'   : $_GET['username'];
    // $password = (empty($_GET['password'])) ? 'pass1' : $_GET['password'];
    $json = sprintf($json, $username, $username);

    // configure the curl options
    $ch = curl_init("https://rmorgan-latest.cafex.com:8443/gateway/sessions/session");
    curl_setopt($ch,CURLOPT_POST, true);
    curl_setopt($ch,CURLOPT_POSTFIELDS, $json);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);    
    curl_setopt($ch, CURLOPT_HTTPHEADER, [         
        'Content-Type: application/json',
        'Content-Length: ' . strlen($json)
    ]);

    // execute HTTP POST & close the connection
    $response = curl_exec($ch);
    curl_close($ch);

    // decode the JSON and pick out the session token
    $decodedJson = json_decode($response);
    $id = $decodedJson->{'sessionid'};

    // echo the ID we've retrieved
    echo $id;

?>
